
Create a service and add the endpoint 'endpoint' to it.

This module only supports the create method for now.

use it by calling url:
http://localhost/SERVICE/endpoint.json

and pass a json object as example in the request as POST
(right now, the callback function does nothing)

	{
		"type":"Title",
		"title":"Exaple Title",
		"body":{
			"und":[
				{
					"value":"This is the body of the article."
				}
			]
		}
	}